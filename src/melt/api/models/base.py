from sqlalchemy.types import TypeDecorator, CHAR
from sqlalchemy.dialects.postgresql import UUID
import uuid
from app import db

class GUID(TypeDecorator):
  """Platform-independent GUID type.

  Uses Postgresql's UUID type, otherwise uses
  CHAR(32), storing as stringified hex values.

  """
  impl = CHAR

  def load_dialect_impl(self, dialect):
    if dialect.name == 'postgresql':
      return dialect.type_descriptor(UUID())
    else:
      return dialect.type_descriptor(CHAR(32))

  def process_bind_param(self, value, dialect):
    if value is None:
      return value
    elif dialect.name == 'postgresql':
      return str(value)
    else:
      if not isinstance(value, uuid.UUID):
        return "%.32x" % uuid.UUID(value).int
      else:
        # hexstring
        return "%.32x" % value.int

  def process_result_value(self, value, dialect):
    if value is None:
      return value
    else:
      return uuid.UUID(value)

class Base(db.Model):
  __abstract__  = True

  id = db.Column(db.Integer, primary_key=True)
  date_created  = db.Column(db.DateTime,  default=db.func.current_timestamp())
  date_modified = db.Column(db.DateTime,  default=db.func.current_timestamp(),
                                         onupdate=db.func.current_timestamp())